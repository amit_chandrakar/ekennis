<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v3.12.1, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v3.12.1, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="assets/images/logo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>ekennis</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&amp;subset=latin">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
  <link rel="stylesheet" href="assets/bootstrap-material-design-font/css/material.css">
  <link rel="stylesheet" href="assets/et-line-font-plugin/style.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/animate.css/animate.min.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
<section id="ext_menu-1">

    <nav class="navbar navbar-dropdown navbar-fixed-top">
        <div class="container">

            <div class="mbr-table">
                <div class="mbr-table-cell">

                    <div class="navbar-brand">
                        <span class="navbar-logo"><img src="assets/images/logo-dark-332x128.png"></span>
                        
                    </div>

                </div>
                <div class="mbr-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="hamburger-icon"></div>
                    </button>

                    <ul class="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar"><li class="nav-item"><a class="nav-link link" href="index.php">HOME</a></li><li class="nav-item"><a class="nav-link link" href="https://ekennis.matrixlms.com">LMS</a></li><li class="nav-item dropdown"><a class="nav-link link dropdown-toggle" href="#" data-toggle="dropdown-submenu" aria-expanded="false">COURSE</a><div class="dropdown-menu"><a class="dropdown-item" href="vocational.php">Vocational Training</a><a class="dropdown-item" href="BPM.php">Business Process Management<br></a><a class="dropdown-item" href="corporate.php">Corporate Training</a><a class="dropdown-item" href="SoftSkill.php">Soft Skill Training&nbsp;</a><a class="dropdown-item" href="PITC.php">Professinoal IT Certification&nbsp;</a><a class="dropdown-item" href="LBYG.php">Learn by Gamification</a></div></li><li class="nav-item"><a class="nav-link link" href="contect.php">CONTACT</a></li><li class="nav-item nav-btn"><a class="nav-link btn btn-secondary-outline btn-secondary" data-toggle="modal" data-target="#myModal">APPLY HERE</a></li></ul>
                    <button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="close-icon"></div>
                    </button>

                </div>
            </div>

        </div>
    </nav>

</section>

<section class="engine"></section><section class="mbr-slider mbr-section mbr-section__container carousel slide mbr-section-nopadding mbr-after-navbar" data-ride="carousel" data-keyboard="false" data-wrap="true" data-pause="false" data-interval="4000" id="slider-2">
    <div>
        <div>
            <div>
                
                <div class="carousel-inner" role="listbox">
                    <div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full active" data-bg-video-slide="false" style="background-image: url(assets/images/bg-2000x894.jpg);">
                        <div class="mbr-table-cell">
                            <div class="mbr-overlay" style="opacity: 0.3;"></div>
                            <div class="container-slide container">
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-1">
                                        <h2 class="mbr-section-title display-1"><br>A Better Education <br>Center</h2>
                                        <p class="mbr-section-lead lead">Training that emphasizes skills and knowledge required for a particular job function or a trade.</p>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full" data-bg-video-slide="false" style="background-image: url(assets/images/bg-2000x800.jpg);">
                        <div class="mbr-table-cell">
                            <div class="mbr-overlay" style="opacity: 0.3;"></div>
                            <div class="container-slide container">
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-1">
                                        <h2 class="mbr-section-title display-1">Ekennis&nbsp;<br>Learn Better</h2>
                                        <p class="mbr-section-lead lead">Ekennis is a learning and education center where you can learn in better way...</p>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><div class="mbr-section mbr-section-hero carousel-item dark center mbr-section-full" data-bg-video-slide="false" style="background-image: url(assets/images/bg-2000x973.jpg);">
                        <div class="mbr-table-cell">
                            <div class="mbr-overlay" style="opacity: 0.4;"></div>
                            <div class="container-slide container">
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-1">
                                        <h2 class="mbr-section-title display-1"><p><span style="line-height: 1.1;"><br></span></p><p><span style="line-height: 1.1;">Create Everyhting Easly&nbsp;</span><br><span style="line-height: 1.1;">With Ekennis<br></span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; A &nbsp;trusted &nbsp;brand...</p></h2>
                                        <p class="mbr-section-lead lead"></p>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <a data-app-prevent-settings="" class="left carousel-control" role="button" data-slide="prev" href="#slider-2">
                    <span class="icon-prev" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a data-app-prevent-settings="" class="right carousel-control" role="button" data-slide="next" href="#slider-2">
                    <span class="icon-next" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section mbr-section__container" id="buttons1-3" style="background-color: rgb(255, 255, 255); padding-top: 40px; padding-bottom: 0px;">

</section>

<section class="mbr-cards mbr-section mbr-section-nopadding" id="features3-4" style="background-color: rgb(255, 255, 255);">

    

    <div class="mbr-cards-row row">
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 40px; padding-bottom: 80px;">
            <div class="container">
              <div class="card cart-block">
                  <div class="card-img"><img src="assets/images/vocational-600x389.jpg" class="card-img-top"></div>
                  <div class="card-block">
                    <h4 class="card-title">Vocational Trainning</h4>
                    <h5 class="card-subtitle">&nbsp;Provides job-specific technical training for work in the trades. These programs generally focus on providing students with hands-on instruction, and can lead to certification, a diploma or certificate.&nbsp;&nbsp;<br><br></h5>
                    
                    <div class="card-btn"><a href="vocational.php" class="btn btn-primary">Details</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 40px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="assets/images/capture-enabled-business-process-management-600x400.jpg" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">Business Process Management</h4>
                        <h5 class="card-subtitle">Systematic approach to making an organization's workflow more effective, more efficient and more capable of adapting to an ever-changing environment.<br><br><br></h5>
                        
                        <div class="card-btn"><a href="BPM.php" class="btn btn-primary">Details</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 40px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="assets/images/training-photo-600x399.jpg" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">Corporate Training</h4>
                        <h5 class="card-subtitle">Corporate Education refers to a system of professional development activities provided to educate employees<br><br><br><br><br><br></h5>
                        
                        <div class="card-btn"><a href="corporate.php" class="btn btn-primary">Details</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 40px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="assets/images/bigstock-soft-skills-88115135-e1445430741693-600x417.jpg" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">Soft Skill Training</h4>
                        <h5 class="card-subtitle">&nbsp;Combination of interpersonal people skills, social skills, communication skills, character traits, attitudes, career attributes, social intelligence and emotional intelligence quotients<br><br><br></h5>
                        
                        <div class="card-btn"><a href="SoftSkill.php" class="btn btn-primary">Details</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 40px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="assets/images/project-management-professional-training-600x400.jpg" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">Professional IT Certification</h4>
                        <h5 class="card-subtitle">Designation earned by a person to assure qualification to perform a job or task<br><br><br><br><br><br></h5>
                        
                        <div class="card-btn"><a href="PITC.php" class="btn btn-primary">Details</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-2" style="padding-top: 40px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img"><img src="assets/images/gamification-600x382.png" class="card-img-top"></div>
                    <div class="card-block">
                        <h4 class="card-title">Learn by Gamification</h4>
                        <h5 class="card-subtitle">The gamification of learning is an educational approach to motivate students to learn by using game elements or<div>Online learning environments.</div><div><br></div><div><br></div></h5>
                        
                        <div class="card-btn"><a href="LBYG.php" class="btn btn-primary">Details</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section" id="msg-box5-7" style="background-color: rgb(255, 255, 255); padding-top: 0px; padding-bottom: 40px;">

    
    <div class="container">
        <div class="row">
            <div class="mbr-table-md-up">

              

              <div class="mbr-table-cell col-md-5 text-xs-center text-md-right content-size">
                  <h3 class="mbr-section-title display-2">Welcome to Ekennis Education Center</h3>
                  <div class="lead">

                    <p>Our Vision to facilitate transformation of students into good human beings, responsible citizens and competent professionals.</p>

                  </div>

                  <div><a class="btn btn-primary" href="contect.php">CONTACT US&nbsp;</a></div>
              </div>


              


              <div class="mbr-table-cell mbr-left-padding-md-up mbr-valign-top col-md-7 image-size" style="width: 50%;">
                  <div class="mbr-figure"><img src="assets/images/logo-dark-1400x538.png"></div>
              </div>

            </div>
        </div>
    </div>

</section>

<section class="mbr-section mbr-parallax-background" id="testimonials3-b" style="background-image: url(assets/images/g8-2000x1210.jpg); padding-top: 40px; padding-bottom: 40px;">

    <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(40, 50, 78);">
    </div>

        <div class="mbr-section mbr-section__container mbr-section__container--middle">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-xs-center">
                        <h3 class="mbr-section-title display-2">WHAT STUDENTS SAY</h3>
                        
                    </div>
                </div>
            </div>
        </div>


    <div class="mbr-testimonials mbr-section mbr-section-nopadding">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card">
                        <div class="card-block"><p>“They work beyond perfection on SAP technology . Thanks to their SAP instructor . Thanks to Ekennis. Execellent and very professional. I would recommend this course to all the job seekers.”</p><p><br></p></div>
                        <div class="mbr-author card-footer">
                            
                            <div class="mbr-author-name">Chandrahas Verma</div>
                            
                        </div>
                    </div>
                </div><div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card">
                        <div class="card-block"><p>“Just wanted to say thankyou  to Ekennis team . I am very pleased with the services and specially with the classes what they conduct."</p><p>Will recommend ekennis for all.</p><p><br></p></div>
                        <div class="mbr-author card-footer">
                            
                            <div class="mbr-author-name">Amit Chandrakar</div>
                            
                        </div>
                    </div>
                </div><div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card">
                        <div class="card-block"><p>“Ekennis focuses on overall development of it's students and employees too . they also provides various tools while joining. I am damn sure that if someone do this course he/she will be taken by any good company”</p></div>
                        <div class="mbr-author card-footer">
                            
                            <div class="mbr-author-name">Rajesh Sonber</div>
                            
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</section>

<section class="mbr-section" id="form1-c" style="background-color: rgb(255, 255, 255); padding-top: 40px; padding-bottom: 40px;">
    
    <div class="mbr-section mbr-section__container mbr-section__container--middle">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-center">
                    <h3 class="mbr-section-title display-2">Leave a Message...</h3>
                    <small class="mbr-section-subtitle">Enter your message in the space below:</small>
                </div>
            </div>
        </div>
    </div>
    <div class="mbr-section mbr-section-nopadding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1" data-form-type="formoid">


                    <div data-form-alert="true">
                        <div hidden="" data-form-alert-success="true" class="alert alert-form alert-success text-xs-center">Thanks for Message, we will contact you.</div>
                    </div>


                    <form action="https://mobirise.com/" method="post" data-form-title="Leave a Message...">

                        <input type="hidden" value="jdVh1HFVuYNg7Vc9ubuZPkXHkdOOZ0xo8wPDnWRQbpOlwc2CgQjtzUb3ZJRoAD8QUeafjH6Fhjp9yOxtINwUjvtF/cdfwywR542vyt9kHJSPRJ2rUI9/3ycZmiFiU+/r" data-form-email="true">

                        <div class="row row-sm-offset">

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-c-name">Name<span class="form-asterisk">*</span></label>
                                    <input type="text" class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="name" required="" data-form-field="Name" id="form1-c-name">
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-c-email">Email<span class="form-asterisk">*</span></label>
                                    <input type="email" class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="email" required="" data-form-field="Email" id="form1-c-email">
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-c-phone">Phone</label>
                                    <input type="tel" class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="phone" data-form-field="Phone" id="form1-c-phone">
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="form1-c-message">Message</label>
                            <textarea class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="message" rows="7" data-form-field="Message" id="form1-c-message"></textarea>
                        </div>

                        <div><button type="submit" class="btn btn-primary">CONTACT US</button></div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-footer mbr-section mbr-section-md-padding" id="contacts3-d" style="background-color: rgb(46, 46, 46); padding-top: 90px; padding-bottom: 30px;">


    <div class="row">
    
            <div class="mbr-company col-xs-12 col-md-6 col-lg-3">

                <div class="mbr-company card">
                    <div><img src="assets/images/untitled-265x122.png" class="card-img-top"></div>
                    <div class="card-block">
                        <p class="card-text">Training that emphasizes skills and knowledge required for a particular job function or a trade.</p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <span class="list-group-icon"><span class="etl-icon icon-phone mbr-iconfont-company-contacts3"></span></span>
                            <span class="list-group-text">+91-9206059605</span>
                        </li>
                        <li class="list-group-item">
                            <span class="list-group-icon"><span class="etl-icon icon-map-pin mbr-iconfont-company-contacts3"></span></span>
                            <span class="list-group-text">Concord Anthuriams, 3rd floor,&nbsp;<br>Electronic City Phase-1, Bangalore, Karnataka-560100, India</span>
                        </li>
                        <li class="list-group-item active">
                            <span class="list-group-icon"><span class="etl-icon icon-envelope mbr-iconfont-company-contacts3"></span></span>
                            <span class="list-group-text" style="color: white;"><a href="mailto:support@mobirise.com"></a>support@ekennis.com</span>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="mbr-footer-content col-xs-12 col-md-6 col-lg-3">
                Course Categories
                 <ul><li><a class="text-white" href="vocational.php">Vocational Trainning</a></li><li><a class="text-white" href="BPM.php">Business Process management</a></li><li><a class="text-white" href="corporate.php">Corporate Training</a></li><li><a class="text-white" href="SoftSkill.php">Soft Skill Training</a></li><li><a class="text-white" href="PITC.php">Professional IT Certification</a></li><li><a class="text-white" href="LBYG.php">Learn By Gamification</a></li></ul>
           </div>
            <div class="mbr-footer-content col-xs-12 col-md-6 col-lg-3">
                <p>Contacts :-<br>Email: support@ekennis.com<br>Phone: +91-9206059605<br><br><br>Address :-<br>Concord Anthuriams, 3rd floor, <br>Electronic City Phase-1, Bangalore, Karnataka-560100, India<br></p>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3" data-form-type="formoid">

                <div data-form-alert="true">
                    <div hidden="" data-form-alert-success="true">Successfully Send.</div>
                </div>

                <form action="https://mobirise.com/" method="post" data-form-title="MESSAGE">

                    <input type="hidden" value="1WS704tl+VQ9wtat1aMTB8sgbYKC96KCk9rwgPaA8Tkz6QlrnvufKy6ehXb+K135pawfRjZ80nClpRVzm/j664Dtaf4/Kxgh02yWqHSx9mCx4o9XOHw8UdUlpN2+ioCW" data-form-email="true">

                    

                    <div class="form-group">
                        <label class="form-control-label" for="contacts3-d-email">Email<span class="form-asterisk">*</span></label>
                        <input type="email" class="form-control input-sm input-inverse" name="email" required="" data-form-field="Email" id="contacts3-d-email">
                    </div>

                    

                    <div class="form-group">
                        <label class="form-control-label" for="contacts3-d-message">Queries</label>
                        <textarea class="form-control input-sm input-inverse" name="message" data-form-field="Message" rows="5" id="contacts3-d-message"></textarea>
                    </div>

                    <div><button type="submit" class="btn btn-black">SEND</button></div>

                </form>

            </div>
        </div>
</section>


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" >
    
      <!-- Modal content-->
      <div class="modal-content" style="color: black ;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h5 class="modal-title">Registration</h5>
        </div>


<section class="mbr-section" id="form1-c" style="background-color: rgb(255, 255, 255); padding-top: 40px; padding-bottom: 40px;">
    
    <div class="mbr-section mbr-section-nopadding">
        <div class="container">
            <div class="row">
                <div data-form-type="formoid">


                    <div data-form-alert="true">
                        <div hidden="" data-form-alert-success="true" class="alert alert-form alert-success text-xs-center">Thanks for Message, we will contact you.</div>
                    </div>



 <div class="modal-body">

                    <form action="https://mobirise.com/" method="post" data-form-title="Leave a Message...">

                        <input type="hidden" value="jdVh1HFVuYNg7Vc9ubuZPkXHkdOOZ0xo8wPDnWRQbpOlwc2CgQjtzUb3ZJRoAD8QUeafjH6Fhjp9yOxtINwUjvtF/cdfwywR542vyt9kHJSPRJ2rUI9/3ycZmiFiU+/r" data-form-email="true">

                        <div class="row row-sm-offset">

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-c-name">Name<span class="form-asterisk">*</span></label>
                                    <input type="text" class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="name" required="" data-form-field="Name" id="form1-c-name">
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-c-email">Email<span class="form-asterisk">*</span></label>
                                    <input type="email" class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="email" required="" data-form-field="Email" id="form1-c-email">
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="form1-c-phone">Phone</label>
                                    <input type="tel" class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="phone" data-form-field="Phone" id="form1-c-phone">
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="form1-c-message">Courses you are applying for</label>
                            <textarea class="form-control" style="border: 1px solid #ADCCCC;color:#2F4F4F" name="message" rows="7" data-form-field="Message" id="form1-c-message"></textarea>
                        </div>

                        <div><button type="submit" class="btn btn-primary">CONTACT US</button></div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

 <div class="modal-footer">
    <small>Download the Registration Form here</small><a href="ekennis.pdf" download>
    <img border="0" height="30px" width="30px" src="assets/images/pdf.png"></a>
        </div>
      </div>
      
    </div>
  </div>










  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/viewport-checker/jquery.viewportchecker.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/jarallax/jarallax.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/formoid/formoid.min.js"></script>
  
  
  <input name="animation" type="hidden">
   <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon"></i></a></div>
  </body>
</html>